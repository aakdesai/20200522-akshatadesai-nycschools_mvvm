package com.akshata.nycschools.service;

import com.akshata.nycschools.model.SchoolDO;
import com.akshata.nycschools.model.SchoolDetailDO;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Interface for SchoolApi
 */
public interface SchoolApi {

    // For school list
    @GET("s3k6-pzi2.json")
    Call<List<SchoolDO>> getSchools();

    // For school details
    @GET("f9bf-2cp4.json")
    Call<List<SchoolDetailDO>> getSchoolsDetails();
}