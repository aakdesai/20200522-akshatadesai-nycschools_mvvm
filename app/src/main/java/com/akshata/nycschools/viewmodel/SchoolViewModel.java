package com.akshata.nycschools.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.akshata.nycschools.repository.SchoolRepository;
import com.akshata.nycschools.model.SchoolDO;

import java.util.List;

/**
 * School detail ViewModel class
 */
public class SchoolViewModel extends ViewModel {

    /**
     * School Repository
     */
    private SchoolRepository schoolRepository;

    /**
     * Constructor
     */
    public SchoolViewModel() {
        schoolRepository = SchoolRepository.getInstance();
    }

    /**
     * Get list of schools
     *
     * @return
     */
    public LiveData<List<SchoolDO>> getSchools() {
        return schoolRepository.getSchools();
    }
}