package com.akshata.nycschools.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.akshata.nycschools.repository.SchoolRepository;
import com.akshata.nycschools.model.SchoolDetailDO;

import java.util.List;

/**
 * School detail view model class
 */
public class SchoolDetailViewModel extends ViewModel{

    /**
     * School repository
     */
    private SchoolRepository schoolRepository;

    /**
     * Constructor
     */
    public SchoolDetailViewModel() {
        schoolRepository = SchoolRepository.getInstance();
    }

    /**
     * Get school details
     *
     * @return
     */
    public LiveData<List<SchoolDetailDO>> getSchoolsDetail() {
        return schoolRepository.getSchoolSatDetails();
    }
}