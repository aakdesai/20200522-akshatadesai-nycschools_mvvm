package com.akshata.nycschools.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * School model class
 */
public class SchoolDO {

    @SerializedName("school_name")
    @Expose
    private String schoolName;

    @SerializedName("website")
    @Expose
    private String schoolWebsite;

    @SerializedName("dbn")
    @Expose
    private String schoolID;

    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;

    @SerializedName("fax_number")
    @Expose
    private String faxNumber;

    @SerializedName("school_email")
    @Expose
    private String schoolEmail;

    /**
     * Constructor
     *
     * @param schoolName
     * @param schoolWebsite
     * @param schoolID
     * @param phoneNumber
     * @param faxNumber
     * @param schoolEmail
     */
    public SchoolDO(String schoolName, String schoolWebsite, String schoolID, String phoneNumber, String faxNumber, String schoolEmail) {
        this.schoolName = schoolName;
        this.schoolWebsite = schoolWebsite;
        this.schoolID = schoolID;
        this.phoneNumber = phoneNumber;
        this.faxNumber = faxNumber;
        this.schoolEmail = schoolEmail;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolWebsite() {
        return schoolWebsite;
    }

    public void setSchoolWebsite(String schoolWebsite) {
        this.schoolWebsite = schoolWebsite;
    }

    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public String getSchoolEmail() {
        return schoolEmail;
    }

    public void setSchoolEmail(String schoolEmail) {
        this.schoolEmail = schoolEmail;
    }
}