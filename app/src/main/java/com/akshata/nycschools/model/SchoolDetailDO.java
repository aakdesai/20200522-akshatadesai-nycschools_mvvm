package com.akshata.nycschools.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 *  School details model class
 */
public class SchoolDetailDO {

    @SerializedName("school_name")
    @Expose
    private String schoolName;

    @SerializedName("dbn")
    @Expose
    private String schoolID;

    @SerializedName("num_of_sat_test_takers")
    @Expose
    private String satStudents;

    @SerializedName("sat_critical_reading_avg_score")
    @Expose
    private String avgReadingScore;

    @SerializedName("sat_math_avg_score")
    @Expose
    private String avgMathScore;

    @SerializedName("sat_writing_avg_score")
    @Expose
    private String avgWritingScore;

    /**
     * Constructor
     *
     * @param schoolName
     * @param schoolID
     * @param satStudents
     * @param avgReadingScore
     * @param avgMathScore
     * @param avgWritingScore
     */
    public SchoolDetailDO(String schoolName, String schoolID, String satStudents, String avgReadingScore, String avgMathScore, String avgWritingScore) {
        this.schoolName = schoolName;
        this.schoolID = schoolID;
        this.satStudents = satStudents;
        this.avgReadingScore = avgReadingScore;
        this.avgMathScore = avgMathScore;
        this.avgWritingScore = avgWritingScore;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public String getSatStudents() {
        return satStudents;
    }

    public void setSatStudents(String satStudents) {
        this.satStudents = satStudents;
    }

    public String getAvgReadingScore() {
        return avgReadingScore;
    }

    public void setAvgReadingScore(String avgReadingScore) {
        this.avgReadingScore = avgReadingScore;
    }

    public String getAvgMathScore() {
        return avgMathScore;
    }

    public void setAvgMathScore(String avgMathScore) {
        this.avgMathScore = avgMathScore;
    }

    public String getAvgWritingScore() {
        return avgWritingScore;
    }

    public void setAvgWritingScore(String avgWritingScore) {
        this.avgWritingScore = avgWritingScore;
    }
}