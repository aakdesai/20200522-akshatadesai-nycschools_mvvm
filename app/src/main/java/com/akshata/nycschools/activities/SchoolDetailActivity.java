package com.akshata.nycschools.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.akshata.nycschools.R;
import com.akshata.nycschools.viewmodel.SchoolDetailViewModel;
import com.akshata.nycschools.model.SchoolDetailDO;

import java.util.List;

/**
 * SchoolDetailActivity class
 */
public class SchoolDetailActivity extends AppCompatActivity {

    private static final String FAX_SYMBOL = " (F)";
    private static final String PHONE_SYMBOL = " (P)";
    private Bundle bundle;
    private TextView schoolNameTextView, satStudentsTextView, avgReadingTextView,avgWritingTextView,
            avgMathTextView,schoolEmailTextView,phoneNumberTextView,faxNumberTextView;
    private String schoolId,faxNumber,phoneNumber,schoolEmail;
    private TableLayout satTable;
    private LinearLayout schoolDetailsLayout;

    /**
     * onCreate method for school detail activity
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_detail);

        schoolNameTextView = findViewById(R.id.schoolNameTextView);
        schoolEmailTextView = findViewById(R.id.schoolEmailTextView);
        phoneNumberTextView = findViewById(R.id.phoneNumberTextView);
        faxNumberTextView = findViewById(R.id.faxNumberTextView);
        satStudentsTextView = findViewById(R.id.satStudentsTextView);
        avgReadingTextView = findViewById(R.id.avgReadingTextView);
        avgWritingTextView = findViewById(R.id.avgWritingTextView);
        avgMathTextView = findViewById(R.id.avgMathTextView);
        schoolDetailsLayout = findViewById(R.id.schoolDetailsLayout);
        satTable = findViewById(R.id.satTable);

        //initialize viewModel
        SchoolDetailViewModel schoolDetailViewModel = ViewModelProviders.of(this).get(SchoolDetailViewModel.class);
        schoolDetailViewModel.getSchoolsDetail().observe(this, new Observer<List<SchoolDetailDO>>() {
            @Override
            public void onChanged(List<SchoolDetailDO> schoolsDetails) {
                // update UI accordingly
                validateData(schoolsDetails);
            }
        });

        // Create bundle for extracting Intent data from Main Activity
        bundle = new Bundle();
        if (bundle != null){
            schoolId = getIntent().getStringExtra("schoolID");
            faxNumber = getIntent().getStringExtra("faxNumber");
            phoneNumber = getIntent().getStringExtra("phoneNumber");
            schoolEmail = getIntent().getStringExtra("SchoolEmail");
        }
    }

    /**
     * Set data in UI components
     *
     * @param schoolDetailDO
     */
    public void setSchoolSatDetails(SchoolDetailDO schoolDetailDO){
            schoolNameTextView.setText(schoolDetailDO.getSchoolName());
            satStudentsTextView.setText(schoolDetailDO.getSatStudents());
            avgReadingTextView.setText(schoolDetailDO.getAvgReadingScore());
            avgWritingTextView.setText(schoolDetailDO.getAvgWritingScore());
            avgMathTextView.setText(schoolDetailDO.getAvgMathScore());
    }

    /**
     * Set school description
     */
    public void setSchoolDesc(){
        if (schoolEmail == null) {
            schoolEmailTextView.setVisibility(View.GONE);
        } else {
            schoolEmailTextView.setText(schoolEmail);
        }
        if (phoneNumber == null) {
            phoneNumberTextView.setVisibility(View.GONE);
        } else {
            phoneNumberTextView.setText(phoneNumber + PHONE_SYMBOL);
        }
        if (faxNumber == null) {
            faxNumberTextView.setVisibility(View.GONE);
        } else {
            faxNumberTextView.setText(faxNumber + FAX_SYMBOL);
        }
    }

    /**
     * Validate data
     *
     * @param schoolsDetails
     */
    public void validateData(List<SchoolDetailDO> schoolsDetails){
        Boolean studentDetailAvailable= false;
        for (SchoolDetailDO schoolDetailDO : schoolsDetails){
            if (schoolDetailDO != null && schoolId.equals(schoolDetailDO.getSchoolID())){
                setSchoolSatDetails(schoolDetailDO);
                setSchoolDesc();
                studentDetailAvailable = true;
                break;
            }
        }
        if(!studentDetailAvailable){
            schoolNameTextView.setText(R.string.data_not_found);
            satTable.setVisibility(View.GONE);
            schoolDetailsLayout.setVisibility(View.GONE);
        }
    }
}