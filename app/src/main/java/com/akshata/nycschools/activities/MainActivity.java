package com.akshata.nycschools.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.akshata.nycschools.R;
import com.akshata.nycschools.viewmodel.SchoolViewModel;
import com.akshata.nycschools.adapters.SchoolAdapter;
import com.akshata.nycschools.model.SchoolDO;

import java.util.List;

/**
 * MainActivity class
 */
public class MainActivity extends AppCompatActivity {

    private SchoolAdapter schoolAdapter;
    private List<SchoolDO> schools;
    private RecyclerView recyclerView;

    /**
     * OnCreate
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Preparing recyclerview
        recyclerView = (RecyclerView) findViewById(R.id.schoolRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //initialize viewModel
        SchoolViewModel schoolViewModel = ViewModelProviders.of(this).get(SchoolViewModel.class);
        schoolViewModel.getSchools().observe(this, new Observer<List<SchoolDO>>() {
            @Override
            public void onChanged(List<SchoolDO> schools) {
                schoolAdapter = new SchoolAdapter(schools);
                recyclerView.setAdapter(schoolAdapter);
                schoolAdapter.setOnSchoolClickListener(new SchoolAdapter.OnSchoolClickListener() {
                    @Override
                    public void onSchoolClick(SchoolDO schoolDO) {
                        Intent intent = new Intent(MainActivity.this, SchoolDetailActivity.class);
                        intent.putExtra("schoolID", schoolDO.getSchoolID());
                        intent.putExtra("faxNumber", schoolDO.getFaxNumber());
                        intent.putExtra("phoneNumber", schoolDO.getPhoneNumber());
                        intent.putExtra("SchoolEmail", schoolDO.getSchoolEmail());
                        startActivity(intent);
                    }
                });
            }
        });
    }
}