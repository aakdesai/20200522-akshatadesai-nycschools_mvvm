package com.akshata.nycschools.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.akshata.nycschools.model.SchoolDO;
import com.akshata.nycschools.model.SchoolDetailDO;
import com.akshata.nycschools.service.SchoolApi;
import com.akshata.nycschools.service.ServiceGenerator;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * School repository class
 */
public class SchoolRepository {
    // School Api interface.
    private SchoolApi schoolApi;
    // School repository
    private static SchoolRepository schoolRepository;

    // Mutable live data for list of schools
    private MutableLiveData<List<SchoolDO>> SchoolInfoMutableLiveData = new MutableLiveData<>();

    // Mutable live data for school detail
    private MutableLiveData<List<SchoolDetailDO>> SchoolDetailMutableLiveData = new MutableLiveData<>();

    public static SchoolRepository getInstance() {
        if (schoolRepository == null) {
            schoolRepository = new SchoolRepository();
        }
        return schoolRepository;
    }

    /**
     * Constructor
     */
    private SchoolRepository() {
        schoolApi = ServiceGenerator.createService(SchoolApi.class);
    }

    /**
     * Get list of schools
     *
     * @return
     */
    public MutableLiveData<List<SchoolDO>> getSchools() {

        schoolApi.getSchools().enqueue(new Callback<List<SchoolDO>>() {
            @Override
            public void onResponse(Call<List<SchoolDO>> call, Response<List<SchoolDO>> response) {
                if (response.isSuccessful()) {
                    SchoolInfoMutableLiveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<SchoolDO>> call, Throwable t) {
                Log.d("Fail", "Failed SchoolDO retrofit");
            }
        });
        return SchoolInfoMutableLiveData;
    }

    /**
     * Get school details
     *
     * @return
     */
    public MutableLiveData<List<SchoolDetailDO>> getSchoolSatDetails() {
        schoolApi.getSchoolsDetails().enqueue(new Callback<List<SchoolDetailDO>>() {
            @Override
            public void onResponse(Call<List<SchoolDetailDO>> call, Response<List<SchoolDetailDO>> response) {
                if (response.isSuccessful()) {
                    SchoolDetailMutableLiveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<SchoolDetailDO>> call, Throwable t) {
                Log.d("Fail", "Failed SchoolDetailDO retrofit");
            }
        });
        return SchoolDetailMutableLiveData;
    }
}