package com.akshata.nycschools.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.akshata.nycschools.R;
import com.akshata.nycschools.model.SchoolDO;

import java.util.List;

/**
 * School adapter class for recyclerview
 */
public class SchoolAdapter extends RecyclerView.Adapter<SchoolAdapter.ViewHolder> {
    private List<SchoolDO> schools;
    private OnSchoolClickListener schoolListener;

    /**
     * Constructor
     *
     * @param schools
     */
    public SchoolAdapter(List<SchoolDO> schools) {
        this.schools = schools;
    }

    /**
     * OnCreate ViewHolder
     *
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.school_list, parent, false);
        return new ViewHolder(view);
    }

    /**
     * OnBind ViewHolder
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final SchoolDO schoolDO = schools.get(position);
        holder.schoolNameTextView.setText(schoolDO.getSchoolName());
        holder.websiteTextView.setText(schoolDO.getSchoolWebsite());
    }

    /**
     * Method to get school list size
     *
     * @return
     */
    @Override
    public int getItemCount() {
        return schools.size();
    }

    /**
     * ViewHolder for recyclerview
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout schoolCardLayout;
        TextView schoolNameTextView;
        TextView websiteTextView;

        /**
         * ViewHolder constructor
         *
         * @param itemView
         */
        public ViewHolder(View itemView) {
            super(itemView);
            schoolNameTextView = (TextView) itemView.findViewById(R.id.schoolNameTextView);
            websiteTextView = (TextView) itemView.findViewById(R.id.websiteTextView);
            schoolCardLayout = (LinearLayout) itemView.findViewById(R.id.schoolCardLayout);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (schoolListener != null && position != RecyclerView.NO_POSITION) {
                        schoolListener.onSchoolClick(schools.get(position));
                    }
                }
            });
        }
    }

    /**
     * Interface for school item event listener
     */
    public interface OnSchoolClickListener {
        void onSchoolClick(SchoolDO schoolDO);
    }

    /**
     * Set on school click listener
     *
     * @param listener
     */
    public void setOnSchoolClickListener(OnSchoolClickListener listener) {
        this.schoolListener = listener;
    }
}