package com.akshata.nycschools;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.Observer;

import com.akshata.nycschools.model.SchoolDO;
import com.akshata.nycschools.repository.SchoolRepository;
import com.akshata.nycschools.service.SchoolApi;
import com.akshata.nycschools.viewmodel.SchoolViewModel;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Test class for School ViewModel
 */
public class TestSchoolViewModel {

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    @Mock
    private SchoolRepository schoolRepository;

    @Mock
    private SchoolApi schoolApi;

    @Mock
    Observer<List<SchoolDO>> observer;

    private SchoolViewModel schoolViewModel;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        schoolViewModel = new SchoolViewModel();
        schoolViewModel.getSchools().observeForever(observer);
    }

    @Test
    public void testNull() {
        when(schoolApi.getSchools()).thenReturn(null);
        assertNotNull(schoolViewModel.getSchools());
        assertTrue(schoolViewModel.getSchools().hasObservers());
    }
}

